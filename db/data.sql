/* Данные для тестирования запросов, предполагается, что БД только что создана
и данные добавляются по порядку*/

insert into place_capture(name) values
    ('Африка'), /* 1 */
    ('Америка'),/* 2 */
    ('Европа')  /* 3 */
    ;

/* Генерация дерева для nested sets */
insert into slave_category(name, lft, rgt) values
    ('root', 1, 8),              /* 1 */
    ('Популярное', 2, 7),        /* 2 */
    ('Для кухни', 3, 6),         /* 3 */
    ('Мытьё посуды', 4, 5)       /* 4 */
    ;

insert into slave(name, gender, age, weight, skin_color,
    place_capture_id, price_per_hour, cost) values
    ('Боб', 'male', 40, 70, 'white', 1, 300, 800),    /* 1 */
    ('Грег', 'male', 40, 65, 'white', 1, 300, 700),   /* 2 */
    ('Джон', 'male', 40, 50, 'white', 1, 300, 500),   /* 3 */
    ('Джоан', 'female', 40, 40, 'white', 1, 300, 800) /* 4 */
    ;

insert into slave_category_link(slave_id, slave_category_id) values
    (1, 3),
    (2, 3),
    (3, 2),

    (4, 3),
    (4, 4)
    ;
